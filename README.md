##OS

- LinuxMint 13 Xcfe ( _Actualizar SO && Reiniciar && Hardware Adicionales_ )

- Theme Xcfe Bluebird ( _Actualizar a Xcfe 4.10_ )

##Utilidades


**xclip**

Utilidad para copiar texto desde archivos y mas al portapaeles desde la linea de comando

	$ sudo apt-get install xclip

**tree**

Utilidad para mostrar la estructura de un directorio desde la consola

	$ sudo apt-get install tree

##Fonts

**Install Dejavu**

	$ git clone git://gist.github.com/1630581.git ~/.fonts/ttf-dejavu-powerline

**Install Consolas**

	$ git clone https://github.com/eugeneching/consolas-powerline-vim.git ~/.fonts/ttf-consolas-powerline

##Programs

**Google Chrome**

	$ wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	$ sudo dpkg -i ./google-chrome*.deb
	$ sudo apt-get -f install


**Jupiter**

	$ sudo add-apt-repository ppa:webupd8team/jupiter
	$ sudo apt-get update
	$ sudo apt-get install jupiter

**Sublime Text**

	$ sudo add-apt-repository ppa:webupd8team/sublime-text-2
	$ sudo apt-get update
	$ sudo apt-get install sublime-text


**Python 2.7.3**

Si cuentas con python instalado por defecto, omite las 3 primeras lineas

	$ sudo add-apt-repository ppa:fkrull/deadsnakes
	$ sudo apt-get update
	$ sudo apt-get install python2.7

Instalamos Setuptools
 
	$ sudo apt-get install python-setuptools


Instalamos pip

	$ sudo easy_install pip

**Nodejs**

	$ sudo apt-get install nodejs
	$ sudo apt-get install npm

Complementos

	$ sudo npm install -g jade
	$ sudo npm install -g coffee-script
	$ sudo npm install -g stylus
	$ sudo npm install -g express
	

**Servidor Lamp**

	$ sudo apt-get install lamp-server^
	$ sudo a2enmod rewrite 	
Instalamos PhpMyAdmin

	$ sudo apt-get install phpmyadmin

	$ sudo ln -s /usr/share/phpmyadmin /var/www/phpmyadmin
o

	$ sudo ln -fvs /etc/phpmyadmin/apache.conf /etc/apache2/conf.d/phpmyadmin.conf	

Si deseamos tener el document root en la carpeta usuario

	$ mkdir ~/www

Editamos el archivo default para cambiar la ruta

	$ sudo gedit /etc/apache2/sites-available/default

Cambiamos `DocumentRoot /var/www/` con la ruta donde creamos la carpeta www `/home/user/www`

Reiniciamos Apache

	$ sudo /etc/init.d/apache2 restart

**Filezilla**
	
	$ sudo apt-get install filezilla

**Git**

	$ sudo apt-get install git

**tmux**
	
	$ sudo apt-get install tmux

**shutter**
( _Capturador de pantallas_ )

	$ sudo apt-get install shutter


**Clipgrab**
( _Alternativa a Atube Catcher_ )

	$ sudo add-apt-repository ppa:clipgrab-team/ppa
	$ sudo apt-get update
	$ sudo apt-get install clipgrab


**VLC**

	$ sudo apt-get install vlc vlc-plugin-pulse mozilla-plugin-vlc